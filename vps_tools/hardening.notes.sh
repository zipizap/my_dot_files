#PROLOGUE: all this should be run as root, otherwise stated

#update && upgrade:
apt-get update && apt-get upgrade -y

#personal-style: let user_x sudo without password
cat >/etc/sudoers.d/user_x <<'EOT'
Defaults:user_x !requiretty
user_x  ALL=(ALL) NOPASSWD:ALL
EOT


#setup etckeeper
#  - it controls /etc changes with a  .git repo, on APT install/upgrade/remove, and with daily CRON job
#  - see http://evilrouters.net/2011/02/18/using-etckeeper-with-git-on-ubuntu/
apt-get -y install git-core
git config --global user.name "root"
git config --global user.email "root@vps4"
apt-get -y install etckeeper
sed '/#VCS="git"/ s/^#//; /VCS="bzr"/ s/^/#/' -i /etc/etckeeper/etckeeper.conf
etckeeper init
etckeeper commit "Initial commit."
#  - basic usage is:
#    # Everyday a cron job wil commit changes in /etc into /etc/.git
#    # After editing  some /etc file, you can commit manually with:
#    cd /etc
#    sudo etckeeper commit "apache2 security hardened"
#    # You can see the changes of previous commits
#    sudo git status
#    sudo git log
#    sudo git log --oneline
#    sudo git log --summary
#    sudo git log -1 -p
#    # Use this as a *read-only* tool to see changes, but dont change the working directory (like go back and forth between commit changing /etc) or it will mess up your system! be read-only ! 

#sshd hardening
# .change port
sed 's/^#Port 22$/Port 122/g' -i /etc/ssh/sshd_config
# .disable root login (this avoids a LOT of brute-force attempts...)
sed 's/^#PermitRootLogin.*/PermitRootLogin no/g' -i /etc/ssh/sshd_config
# .disable password logint
sed 's/^#PasswordAuthentication.*/PasswordAuthentication no/g' -i /etc/ssh/sshd_config
# .only allow users which belong to group ssh_allow
addgroup ssh_allow
adduser user_x ssh_allow
echo '# :) Only allow login to users belonging to group ssh_allow
AllowGroups ssh_allow
' >> /etc/ssh/sshd_config
systemctl restart ssh


#Setup fail2ban
apt-get -y install fail2ban
cat <<'EOT' > /etc/fail2ban/jail.local
[DEFAULT]
# If IP-X fails "maxretry"-times in less than "bantime"-seconds, then IP-X will be banned during "bantime"-seconds in all-ports
bantime  = 1h
findtime = 30m
maxretry = 5
banaction = iptables-allports

[sshd]
enable = true
port    = 122
backend = systemd
EOT
systemctl restart fail2ban
systemctl status fail2ban


#install public key for user "pete" from remote-host into server "vpsserver", for passwordless ssh connection
#to be run in the remote-host
remote-host$ ssh-copy-id pete@vpsserver

#apt: assume "yes" to all questions
echo 'APT::Get::Assume-Yes "true";' >> /etc/apt/apt.conf.d/99apt_get_assume_yes
chmod 644 /etc/apt/apt.conf.d/99apt_get_assume_yes

#apt: setup automatic install of security updates every day
## POS-NOTE:
##   - this will install only security updates, it will not do normal updates (it could)
##   - if needed, it will reboot after upgrading (like on kernel upgrades)
##   - check logs of automatic updates in /var/log/unattended-upgrades
apt-get install -y unattended-upgrades
echo '
APT::Periodic::Update-Package-Lists "1";
APT::Periodic::Download-Upgradeable-Packages "1";
APT::Periodic::AutocleanInterval "7";
APT::Periodic::Unattended-Upgrade "1";
' > /etc/apt/apt.conf.d/10periodic
sed '
s_//\(Unattended-Upgrade::Remove-Unused-Dependencies\) "false";_\1 "true";_g
s_//\(Unattended-Upgrade::Automatic-Reboot\) "false";_\1 "true";_g' \
-i /etc/apt/apt.conf.d/50unattended-upgrades

#firewall: leave ssh access and close all other ports
## a) Using UFW (better in lxd hosts, to avoid messing with iptables rules of lxd and net)
ufw limit 122/tcp
ufw enable
ufw status numbered
#ufw delete NUM

#  ## b) Using iptables, for total control (better when there is no docker/lxd inside, for simple setups)
#  ## see https://help.ubuntu.com/community/IptablesHowTo
#  iptables -L -v -n # List existing rules - there should be none, or else think before continuing...
#  iptables -F       # Flush or remove existing rules 
#  iptables -A INPUT -i lo -j ACCEPT                                   # lo
#  iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT    # maintain existing connections (like an active ssh session)
#  
#  iptables -A INPUT -p tcp --dport 22 -j ACCEPT                       # ssh
#  #iptables -A INPUT -p tcp --dport 80 -j ACCEPT                       # http
#  #iptables -A INPUT -p tcp --dport 443 -j ACCEPT                      # https
#  #iptables -A INPUT -p tcp --dport 1234 -j ACCEPT                     # add new ports here as needed. Order matters
#  
#  ### Transmission ###
#  #iptables -A INPUT -p udp --dport 51413 -j ACCEPT                              # transmission peers UDP - to public internet
#  #iptables -A INPUT -p tcp --dport 51413 -j ACCEPT                              # transmission peers TCP - to public internet
#  #iptables -A INPUT -p tcp --dport 9091 -s 192.168.0.0/24 -j ACCEPT             # transmission rpc       - only local network
#  
#  ### Ipfs ###
#  #iptables -A INPUT -p tcp --dport 4001 -j ACCEPT                               # IPFS swarm   - to public internet
#  #iptables -A INPUT -p tcp --dport 5001 -s 192.168.0.0/24 -j ACCEPT             # IPFS API     - only local network
#  #iptables -A INPUT -p tcp --dport 8080 -s 192.168.0.0/24 -j ACCEPT             # IPFS gateway - only local network
#  
#  ### Iperf3 ###
#  #iptables -A INPUT -p tcp --dport 5200:5209 -j ACCEPT                          # iperf3 TCP
#  #iptables -A INPUT -p udp --dport 5200:5209 -j ACCEPT                          # iperf3 UDP
#  
#  iptables -A INPUT -j DROP                                           # drop all other
#  iptables -L -v -n
#  ### Now is the moment to check if new connections like ssh will work ok... if not then do iptables -F to start over
#  ### At last, lets save the iptables rules permanently, so they work after reboots
#  iptables-save > /etc/iptables.rules
#  cat << EOT > /etc/network/if-pre-up.d/iptablesload
#  #!/bin/sh
#  iptables-restore < /etc/iptables.rules
#  exit 0
#  EOT
#  cat << EOT > /etc/network/if-post-down.d/iptablessave
#  #!/bin/sh
#  iptables-save > /etc/iptables.rules
#  if [ -f /etc/iptables.downrules ]; then
#     iptables-restore < /etc/iptables.downrules
#  fi
#  exit 0
#  EOT
#  chmod +x /etc/network/if-post-down.d/iptablessave
#  chmod +x /etc/network/if-pre-up.d/iptablesload


#  #secure shared memory
#  # see http://www.thefanclub.co.za/how-to/how-secure-ubuntu-1204-lts-server-part-1-basics
#  # Needs reboot to become effective
#  echo "tmpfs     /dev/shm     tmpfs     defaults,noexec,nosuid     0     0" >> /etc/fstab

#  #secure /bin/su to be used only by group admin
#  dpkg-statoverride --update --add root admin 4750 /bin/su

#Harden network with sysctl settings
#see http://www.thefanclub.co.za/how-to/how-secure-ubuntu-1204-lts-server-part-1-basics
echo '
## :) harden network with sysctl settings
# IP Spoofing protection
net.ipv4.conf.all.rp_filter = 1
net.ipv4.conf.default.rp_filter = 1

# Ignore ICMP broadcast requests
net.ipv4.icmp_echo_ignore_broadcasts = 1

# Disable source packet routing
net.ipv4.conf.all.accept_source_route = 0
net.ipv6.conf.all.accept_source_route = 0
net.ipv4.conf.default.accept_source_route = 0
net.ipv6.conf.default.accept_source_route = 0

# Ignore send redirects
net.ipv4.conf.all.send_redirects = 0
net.ipv4.conf.default.send_redirects = 0

# Block SYN attacks
net.ipv4.tcp_syncookies = 1
net.ipv4.tcp_max_syn_backlog = 2048
net.ipv4.tcp_synack_retries = 2
net.ipv4.tcp_syn_retries = 5

# # Log Martians
# net.ipv4.conf.all.log_martians = 1
# net.ipv4.icmp_ignore_bogus_error_responses = 1

# Ignore ICMP redirects
net.ipv4.conf.all.accept_redirects = 0
net.ipv6.conf.all.accept_redirects = 0
net.ipv4.conf.default.accept_redirects = 0
net.ipv6.conf.default.accept_redirects = 0

# Ignore Directed pings
net.ipv4.icmp_echo_ignore_all = 1
' >> /etc/sysctl.conf

#Prevent IP Spoofing
echo 'nospoof on' >> /etc/host.conf



#final reboot
echo -e "\033[1;31m---- Press ENTER to **reboot**, or CTRL-C to skip ----\033[0m"; read; reboot

