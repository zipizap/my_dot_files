### INSTALL
# gem install wirble awesome_print
###

require 'rubygems'

begin
  require 'wirble'
rescue LoadError
  puts %x(gem install wirble)
end
Wirble::History::DEFAULTS[:history_uniq] = 'reverse'
Wirble.init
Wirble.colorize


#Tab completion, autoident
require 'irb/completion' 
IRB.conf[:AUTO_INDENT] = true 
IRB.conf[:USE_READLINE] = true

#Prompt
IRB.conf[:PROMPT][:CUSTOM] = {
  :PROMPT_I => ">> ",
  :PROMPT_S => "%l>> ",
  :PROMPT_C => ".. ",
  :PROMPT_N => ".. ",
  :RETURN => "=> %s\n"
}

# Set default prompt
IRB.conf[:PROMPT_MODE] = :CUSTOM

##### awesome_print
begin
  require 'ap'
rescue LoadError
  puts %x(gem install awesome_print)
end
unless IRB.version.include?('DietRB')
  IRB::Irb.class_eval do
    def output_value
      ap @context.last_value
    end
  end
else # MacRuby
  IRB.formatter = Class.new(IRB::Formatter) do
    def inspect_object(object)
      object.ai
    end
  end.new
end


