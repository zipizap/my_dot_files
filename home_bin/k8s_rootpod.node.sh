#!/usr/bin/env bash

# .kubectl get node
# .edit LOC29 kubernetes.io/hostname: <your node nade>
# .kubectl apply -f this-file.yaml
# .kubectl exec -ti -n kube-system root-aks-main1-36938559-vmss0001ye -- bash
#   - container will share same namespaces of node:
#     proc, filesystem, net, user (maybe more)
#
AKS_NODE="${1?Usage: $0 aks-main1-36938559-vmss0001ye}"
POD_NAME=rootpod-$AKS_NODE
set -x
cat <<EOT | kubectl apply -f - 
apiVersion: v1
kind: Pod
metadata:
  labels:
    "app.kubernetes.io/component": my-rootpod
    "app.kubernetes.io/instance": my-rootpod
    "app.kubernetes.io/name": my-rootpod
    "app.kubernetes.io/part-of": my-rootpod
    "app.kubernetes.io/version": 2.1.3
    "argocd.argoproj.io/instance": my-rootpod
    "app.kubernetes.io/managed-by": my-rootpod
    run: my-rootpod
  name: $POD_NAME
  namespace: kube-system
spec:
  restartPolicy: "Never"
  terminationGracePeriodSeconds: 0
  hostPID: true
  hostIPC: true
  hostNetwork: true
  tolerations: 
  - operator: "Exists" 
  nodeSelector: 
    kubernetes.io/hostname: $AKS_NODE
  containers:
  - name: "shell"
    image: "docker.io/alpine"
    command: ["nsenter"]
    args: ["-t", "1", "-m", "-u", "-i", "-n", "sleep", "14000"] 
    securityContext:
      privileged: true
EOT
sleep 20
kubectl exec -ti -n kube-system $POD_NAME -- bash
kubectl delete pod $POD_NAME -n kube-system --force --grace-period=0
echo "All done - exiting now"
