set -x
URL="${1?Usage $0 https://www.youtube.com/watch?v=Hz30YoWEhdY}"
cd ~/mp3
youtube-dl --no-check-certificates --extract-audio --audio-format mp3 "${URL}"
