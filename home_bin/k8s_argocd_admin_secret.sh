#!/usr/bin/env bash
while :
do
  echo "======"
  kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d; echo
  argocd login --port-forward --plaintext --username admin --password "$(kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath='{.data.password}' | base64 -d)" || sleep 2
  kubectl port-forward -n argocd svc/argocd-server 8080:80
done

