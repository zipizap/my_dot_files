#! /usr/bin/bash
docker \
  run -it \
  -e UID=$(id -u) -e GID=$(id -g) \
  -e OPENAI_API_KEY=${OPENAI_API_KEY} \
  -v $(pwd):/app \
  `F=.gitconfig ; [[ -r ~/"${F}" ]] && echo -v ~/"${F}":/home/dockeruser/"${F}"` \
  aider --model ${OPENAI_GPT4MODEL} --no-pretty ${@}