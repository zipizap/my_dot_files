#!/usr/bin/env bash
set -efu
#set -x

# INSTALL
#  - configure this program global-vars TARGET_xxx bellow
#  - put symlinks in ~/.ssh and ~/.gitconfig   - ex:
#      ln -sv /dev/null ~/.ssh
#      ln -sv /dev/null ~/.gitconfig
#  - run this program one or many times :)
#
# USAGE:
#   $0 <TARGET_SSH_DIR> <TARGET_GITCONFIG_FILE>
#
# EXAMPLE:
#  $0 \
#    ~/bin/private/.ssh_zipizap \
#    ~/bin/private/.ssh_zipizap/.gitconfig

TARGET_SSH_DIR="${1?missing required arg}"; shift 1
TARGET_GITCONFIG_FILE="${1?missing required arg}"; shift 1

switch_sshDir() {
  if [[ -L ~/.ssh ]]; then
    rm -v ~/.ssh
    ln -sfv "${TARGET_SSH_DIR}" ~/.ssh
    ls -lh ~/.ssh 
  else 
    echo "ERROR: ~/.ssh is not symlink as expected!"
    exit 1
  fi
}

switch_gitconfig() {
  if [[ -L ~/.gitconfig ]]; then
    rm -v ~/.gitconfig
    ln -sfv "${TARGET_GITCONFIG_FILE}" ~/.gitconfig
    ls -lh ~/.gitconfig
  else 
    echo "ERROR: ~/.gitconfig is not symlink as expected!"
    exit 2
  fi
}


main() {
  switch_sshDir
  switch_gitconfig
}
main "${@}"



