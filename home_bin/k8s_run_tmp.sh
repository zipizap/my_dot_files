#pod starts interactive, and deletes itself in the end
# -n kube-system to avoid problems with missing-labels and gatekeeer


IMAGE=nginx
COMMAND=/bin/bash

IMAGE=alpine/socat
COMMAND=/bin/sh

IMAGE=curlimages/curl
COMMAND="sh"

IMAGE=bitnami/kubectl
COMMAND=/bin/sh

IMAGE=alpine:edge
COMMAND=/bin/sh
# apk update && apk add curl

IMAGE=bestsecretdockerregistry.azurecr.io/library/alpine:3.14
COMMAND=/bin/sh

kubectl run -n kube-system -ti --rm --restart=Never tmp --image=${IMAGE} --command -- ${COMMAND}
