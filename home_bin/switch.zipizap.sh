#!/usr/bin/env bash
set -efu
#set -x
TARGET_SSH_DIR="$HOME/bin/private/.ssh_zipizap/"
TARGET_GITCONFIG_FILE="$HOME/bin/private/.ssh_zipizap/.gitconfig"
exec bin.switch.profile.sh \
  "${TARGET_SSH_DIR}" \
  "${TARGET_GITCONFIG_FILE}"



