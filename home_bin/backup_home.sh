#!/usr/bin/env bash
# Paulo Aleixo Campos
__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
function shw_info { echo -e '\033[1;34m'"$1"'\033[0m'; }
function error { echo "ERROR in ${1}"; exit 99; }
trap 'error $LINENO' ERR
#exec > >(tee -i /tmp/$(date +%Y%m%d%H%M%S.%N)__$(basename $0).log ) 2>&1
PS4='████████████████████████${BASH_SOURCE}@${FUNCNAME[0]:-}[${LINENO}]>  '
set -o errexit
set -o pipefail
set -o nounset
shopt -s inherit_errexit
set -o xtrace

main() {
  # parse args - ARGx will be "" or have its value
  local OrigArgs="${@}"
  local YYYYMMDDhhmmss=$(date +%Y%m%d%H%M%S)
  local HOME_DIR_NAME='ub'
  local BACKUP_DIR=/t/local/vboxs/VMs/ub/backups
  local BACKUP_FILE_FULLPATH="${BACKUP_DIR}/ub@lnx.${YYYYMMDDhhmmss}.tgz"
  shw_info ">> Creating new backup '${BACKUP_FILE_FULLPATH}'"
  cd /home 
  set +o errexit
  sudo tar \
    --exclude "${HOME_DIR_NAME}/.cache" \
    --exclude "${HOME_DIR_NAME}/.config/google-chrome" \
    --exclude "${HOME_DIR_NAME}/.config/Postman" \
    --exclude "${HOME_DIR_NAME}/.config/Code/Cache" \
    --exclude "${HOME_DIR_NAME}/.config/Code/CachedData" \
    --exclude "${HOME_DIR_NAME}/configs/golangs/pkg" \
    --exclude "${HOME_DIR_NAME}/configs/vscode_portable" \
    --exclude "${HOME_DIR_NAME}/.kube/cache" \
    --exclude "${HOME_DIR_NAME}/.kube/http-cache" \
    --exclude "${HOME_DIR_NAME}/.mp3" \
    --exclude "${HOME_DIR_NAME}/.Downloads" \
    --exclude "${HOME_DIR_NAME}/things/2022/06june/06_gitlab_runner/09.jun.azureTr" \
    -cf "${BACKUP_FILE_FULLPATH}" "${HOME_DIR_NAME}"
  local TAR_RC=$?
  if [[ "${TAR_RC}" -eq 0 ]]
  then
    # tar return-value is 0, all good, do nothing and let execution continue
    :
  elif [[ "${TAR_RC}" -eq 1 ]]
  then
    # tar return-value 1 happens sometimes when "file changed as we read it"
    # Lets accept return-value 1 as good :) and let execution continue
    # Refs:
    #  - man tar, "RETURN VALUE"
    #  - https://stackoverflow.com/questions/20318852/tar-file-changed-as-we-read-it
    :
  else  
    # tar return-code is something unexpected... error out
    error "Tar return-code '$TAR_RC' is unexpected... erroring-out..."
  fi
  set -o errexit
  ls -lrt "${BACKUP_DIR}"

  NUM_RECENT_BACKUPS_TO_KEEP=3
  shw_info ">> Keeping only $NUM_RECENT_BACKUPS_TO_KEEP most recent backups and cleaning older ones"
  cd "${BACKUP_DIR}"
  ls -1t ub@lnx.*.tgz \
    | tail --lines=+"$((NUM_RECENT_BACKUPS_TO_KEEP + 1))" \
    | xargs rm -vf || true  
  shw_info "Backup completed successfully! Check ${BACKUP_FILE_FULLPATH}"
  ls -lrth "${BACKUP_DIR}"

  shw_info "
== $(basename $0) ${OrigArgs}
== Finished successfully =="

}
main "${@}"
