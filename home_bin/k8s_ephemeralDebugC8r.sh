#!/usr/bin/env bash
set -xefu
NS=kube-system
PODNAME=coredns-59b6bf8b4f-bbf26
C8RNAME=coredns
kubectl debug -it -c debugger --target=$C8RNAME --image=busybox -n $NS $PODNAME
