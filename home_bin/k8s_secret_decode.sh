#!/usr/bin/env bash
MY_NS="${1?Usage: $0 myns mysecret}"  ; shift 1
MY_SECRET="${1?missing secret}"  ; shift 1
set -x
kubectl get secret -n "${MY_NS}" "${MY_SECRET}" -o go-template='{{range $k,$v := .data}}{{"### "}}{{$k}}{{"\n"}}{{$v|base64decode}}{{"\n\n"}}{{end}}'
