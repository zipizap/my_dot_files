#!/usr/bin/env bash
kubectl cluster-info | grep 'control plane' | cut -d'/' -f3 | cut -d. -f1 | sed 's/-[^-]\+$//g'
argocd login --port-forward --plaintext --username admin --password "$(kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath='{.data.password}' | base64 -d)"
argocd app list --port-forward --port-forward-namespace argocd \
| egrep 'NAME|argocd|cert-manager|csi-secret-store-provider-azure|dns|dns-env|ingress-nginx-internal|ingress-nginx-external' \
| hl  -G Healthy -G Synced 
