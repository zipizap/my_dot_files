#!/usr/bin/env bash
kubectl get pdb -A | grep -v NAMESPACE | awk '{ print $1 , $2 }' | xargs -I{} echo kubectl delete pdb -n {} 
