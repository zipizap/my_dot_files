#!/usr/bin/env bash
# Paulo Aleixo Campos
__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
function shw_info { echo -e '\033[1;34m'"$1"'\033[0m'; }
function error { echo "ERROR in ${1}"; exit 99; }
#exec > >(tee -i /tmp/$(date +%Y%m%d%H%M%S.%N)__$(basename $0).log ) 2>&1
PS4='████████████████████████${BASH_SOURCE}@${FUNCNAME[0]:-}[${LINENO}]>  '
set -o errexit; trap 'error $LINENO' ERR
#set +o errexit; trap - ERR
set -o pipefail
set -o nounset
set -o xtrace


main() {
  cd "${__dir}"
  # parse args - ARGx will be "" or have its value
  OrigArgs="${@}"
  local CLUSTER_NAME="${1:?Usage: $0 bs252-t-euw-aks-breeze00}"
  #local ARG2="${1:-}"  ; [[ "${1:-}" ]] && shift

  local CLUSTER_RG="${CLUSTER_NAME}-rg"
  az group delete --name "${CLUSTER_RG}" --yes

  shw_info "
== $(basename $0) ${OrigArgs}
== Finished successfully =="

}
main "${@}"
