#
#set -x
OLD="AssemblyAllCollections"
NEW="CollectionsAllAssembly"
echo "-------- Before --------"
grep -r "${OLD}"
grep -r "${OLD}" | cut -d: -f1 | xargs -I{}  sed "s/${OLD}/${NEW}/g" -i {}
echo "-------- After --------"
grep -r "${OLD}"

