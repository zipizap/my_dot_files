#!/bin/bash

# Install: copy/paste && uncomment into ~/.bashrc the following lines
#
#  # :) My alias loading...
#  ALIAS_FILE_TO_BE_SOURCED="$HOME/Dropbox/Refs/Bash/alias/souce_alias.sh"
#  [ -r  $ALIAS_FILE_TO_BE_SOURCED ] && source $ALIAS_FILE_TO_BE_SOURCED



ALIAS_DIR="$(dirname $BASH_SOURCE)"
TMP_FILE=$(mktemp)
echo -e "\033[34m"":) Loading alias file(s) ...""\033[0m"
find "$ALIAS_DIR" -type f -executable -print0 | \
while read -d $'\0' alias_file; do
  [[ "$alias_file" =~ source_alias\.sh$ ]] && continue
  echo -e "\033[1;30m""  $(basename "$alias_file")""\033[0m"
  echo "source $alias_file" >> $TMP_FILE
done
source $TMP_FILE && rm -rf $TMP_FILE &>/dev/null


