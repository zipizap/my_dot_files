#/bin/bash

grep '^# :) My alias loading' ~/.bashrc || cat >> $HOME/.bashrc <<'EOT'


#------------ :) bash personalization -----------
force_color_prompt=yes
alias ll='ls -alF'
alias '..'='cd ..'

PATH=$HOME/bin:$PATH

# :) Use vim with Ctrl-X+Ctrl-E
export EDITOR=vim

# :) enable colors for man pages (via less)
# See https://www.2daygeek.com/linux-color-man-pages-configuration-less-most-command/#
export LESS_TERMCAP_mb=$'\E[1;31m'     # begin bold
export LESS_TERMCAP_md=$'\E[1;36m'     # begin blink
export LESS_TERMCAP_me=$'\E[0m'        # reset bold/blink
export LESS_TERMCAP_so=$'\E[01;44;33m' # begin reverse video
export LESS_TERMCAP_se=$'\E[0m'        # reset reverse video
export LESS_TERMCAP_us=$'\E[1;32m'     # begin underline
export LESS_TERMCAP_ue=$'\E[0m'        # reset underline
export GROFF_NO_SGR=1                  # for konsole and gnome-terminal


# :) My alias loading...
ALIAS_FILE_TO_BE_SOURCED="WILL_BE_SUBSITUTED/alias/source_alias.sh"
[ -r  $ALIAS_FILE_TO_BE_SOURCED ] && source $ALIAS_FILE_TO_BE_SOURCED

# socket2clipboard
which socket2clipboard &>/dev/null && ( pgrep socket2clip || $(which socket2clipboard) &>/dev/null & ) &>/dev/null

gitacp() {
  git add .
  git commit -m "${1}" && git push
}

## tmp hacky fix
TMUX_CURRENT_SESSION_NAME=$([[ -n "${TMUX+set}" ]] && tmux display-message -p "#S")
if [[ "$TMUX_CURRENT_SESSION_NAME" == "prometheus" ]]; then
  echo ">> TMUX session: $TMUX_CURRENT_SESSION_NAME"
  source /home/rooty/configs/k3d-lab/k3d.source
fi

parse_git_branch() { git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1)/'; }
export PS1="\u@\h \[\e[32m\]\w \[\e[91m\]\$(parse_git_branch)\[\e[00m\]$ "

EOT

sed "s#WILL_BE_SUBSITUTED#""$(dirname $(readlink -f $0))""#g" -i ~/.bashrc
  # NOTE: ALIAS_FILE_TO_BE_SOURCED will be dynamically filled at runtime, and should endup like
  #  /home/xxx/configs/my_dot_files/bash/alias/source_alias.sh




