#!/usr/bin/env bash
# Paulo Aleixo Campos
__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
function shw_info { echo -e '\033[1;34m'"$1"'\033[0m'; }
function error { echo "ERROR in ${1}"; exit 99; }
trap 'error $LINENO' ERR
#exec > >(tee -i /tmp/$(date +%Y%m%d%H%M%S.%N)__$(basename $0).log ) 2>&1
PS4='________________________${BASH_SOURCE}@${FUNCNAME[0]:-}[${LINENO}]>  '
set -o errexit
set -o pipefail
set -o nounset
shopt -s inherit_errexit
set -o xtrace

main() {
  # parse args - ARGx will be "" or have its value
  local OrigArgs="${@}"

  cd "${__dir}"

  # fzf
  sudo apt -y install fzf
    # CTRL-K / CTRL-J (or CTRL-P / CTRL-N) to move cursor up and down
    # Enter key to select the item, CTRL-C / CTRL-G / ESC to exit
    # https://github.com/junegunn/fzf#installation

  ln -s $PWD/DIRdot_oh-my-zsh ~/.oh-my-zsh
  ln -s $PWD/dot_zsh ~/.zshrc
  ln -s $PWD/dot_p10k.zsh ~/.p10k.zsh 
  sudo apt -y install zsh
  chsh -s $(which zsh)


  #### plugins ####
  # Oh-my-zsh provides a plethora of plugins to enhance the terminal experience. The default plugins are in the ~/.oh-my-zsh/plugins directory.
  # To enable an oh-my-zsh plugin, edit the .zshrc configuration and add the plugin name in the plugin’s entry.
  # For example, to enable the git, docker, and npm plugins, we can set the plugin’s entry as:
  #       vim .zshrc
  #       plugins=(git docker npm)
  #
  # TIP: git clone --depth 1 ...
  #
  #   zsh-autosuggestions
  #     - https://github.com/zsh-users/zsh-autosuggestions/blob/master/INSTALL.md
  #     - https://github.com/zsh-users/zsh-autosuggestions#usage
  #     word: <Ctrl-f/b> , line: <arrow-right> , <end>
  #
  #   zsh-syntax-highlighting
  #     - https://github.com/zsh-users/zsh-syntax-highlighting
  #
  #   fzf-zsh-plugin
  #     - https://github.com/unixorn/fzf-zsh-plugin
  #   ! could not make it work well with history searches... removed and back to "fzf" plugin
  
  # #--- deprecated
  #   # now open a new zsh shell and answer more questions
  #   #git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ~/.oh-my-zsh/custom/themes/powerlevel10k
  #   source .zshrc


  shw_info "
== $(basename $0) ${OrigArgs}
== Finished successfully =="

}
main "${@}"




