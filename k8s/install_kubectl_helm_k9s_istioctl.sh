#!/usr/bin/env bash
# Paulo Aleixo Campos
__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
__dbg_on_off=on  # on off
function shw_info { echo -e '\033[1;34m'"$1"'\033[0m'; }
function error { echo "ERROR in ${1}"; exit 99; }
trap 'error $LINENO' ERR
function dbg { [[ "$__dbg_on_off" == "on" ]] || return; echo -e '\033[1;34m'"dbg $(date +%Y%m%d%H%M%S) ${BASH_LINENO[0]}\t: $@"'\033[0m';  }
#exec > >(tee -i /tmp/$(date +%Y%m%d%H%M%S.%N)__$(basename $0).log ) 2>&1
export PS4='\[\e[44m\]\[\e[1;30m\](${BASH_SOURCE}:${LINENO}):${FUNCNAME[0]:+ ${FUNCNAME[0]}():}\[\e[m\] '
set -o errexit
  # NOTE: the "trap ... ERR" alreay stops execution at any error, even when above line is commente-out
set -o pipefail
set -o nounset
set -o xtrace






mkdir -p ~/bin || true

shw_info "kubectl"
cd $(mktemp -d) &&\
curl -LO "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl" &&\
chmod 777 ./kubectl 
mkdir -p ~/bin &&\
mv ./kubectl ~/bin/


shw_info "kubecolor"
curl -Lso /tmp/a.gz https://github.com/hidetatz/kubecolor/releases/download/v0.0.25/kubecolor_0.0.25_Linux_x86_64.tar.gz
(cd ~/bin && tar xvf /tmp/a.gz kubecolor)
cat <<'EOT' >> ~/.bashrc
# kubectl, kubecolor, k
# export KUBECONFIG=$HOME/configs/k3d-lab/kubeconfig.yaml
source <(kubectl completion bash)
#alias k=kubectl
#alias k="kubecolor --force-colors"   # This --force-colors seems to break bash-completion
alias k="kubecolor"
complete -o default -F __start_kubectl k
# ATP: 
#  kubectl  - works as normal, with bash-completion, without colors so its ready for pipes: kubectl logs ... | less
#  k        - calls kubecolor, with bash-completion (although it fails sometimes, try workarounds like "k -n <TAB> ...error ====> k get -n <TAB> ... OK")
#             Limitation: Cant easily pipe colored-output, like: k get all | less -R
EOT



shw_info "helm"
cd $(mktemp -d) &&\
curl -sL "https://get.helm.sh/helm-v3.8.1-linux-amd64.tar.gz" | tar zxv linux-amd64/helm &&\
mv linux-amd64/helm ~/bin/helm 
cat <<'EOT' >> ~/.bashrc
# helm, h
source <(helm completion bash)
alias h=helm
source <(helm completion bash | sed '/__start_helm helm/ s/ helm/ h/g')

EOT



shw_info "k9s (latest :) )"
cd ~/bin &&\
curl -Ls https://api.github.com/repos/derailed/k9s/releases/latest | grep -wo "https.*k9s_Linux_amd64.tar.gz" | xargs curl -sL | tar xvz k9s
ln -sfv "${__dir}/dot_k9s" ~/.k9s
cat <<'EOT' >> ~/.bashrc
## k9s
#alias k9s='k9s --readonly'
#
EOT


# shw_info "istio"
# ISTIO_VERSION=1.9.0
# cd $(mktemp -d) &&\
# curl -L https://istio.io/downloadIstio | TARGET_ARCH=x86_64 sh -
# ISTIO_DIR=$(find $PWD -maxdepth 1 -type d -iname 'istio*')
# cp -v "${ISTIO_DIR}"/bin/istioctl ~/bin
# cp -v "${ISTIO_DIR}"/tools/istioctl.bash ~/bin/istioctl.bash
# cat <<'EOT' >> ~/.bashrc
# # istioctl
# [[ -r ~/bin/istioctl.bash ]] && source istioctl.bash
# 
# EOT


shw_info "kind"
# Available versions: https://github.com/kubernetes-sigs/kind/releases
KIND_VERSION=v0.11.1
mkdir -p ~/bin
curl -Lo ~/bin/kind https://kind.sigs.k8s.io/dl/"${KIND_VERSION}"/kind-linux-amd64
chmod +x ~/bin/kind
cat <<'EOT' >> ~/.bashrc
# kind
source <(~/bin/kind completion bash)

EOT


shw_info "argocd"
# Available versions: https://github.com/kubernetes-sigs/kind/releases
ARGO_VERSION=v2.4.11
mkdir -p ~/bin
curl -Lo ~/bin/argocd https://github.com/argoproj/argo-cd/releases/download/"${ARGO_VERSION}"/argocd-linux-amd64
chmod +x ~/bin/argocd
cat <<'EOT' >> ~/.bashrc
# argocd
source <(~/bin/argocd completion bash)

EOT


shw_info "argocd-rollouts"
# Available versions: https://github.com/kubernetes-sigs/kind/releases
mkdir -p ~/bin
curl -Lo ~/bin/kubectl-argo-rollouts https://github.com/argoproj/argo-rollouts/releases/latest/download/kubectl-argo-rollouts-linux-amd64
chmod +x ~/bin/kubectl-argo-rollouts

shw_info "== Execution complete =="

