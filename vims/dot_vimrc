﻿" This file should be always read/write (encoded) as UTF-8 with BOM 
" If you happen to need to convert this file (or any other) from whatever encoding into UTF-8 BOM, then:
"   " open it with vim, and
"   :set fileencoding         " (Show) current fileencoding
"   :set fileencoding=utf-8   " (Change)  Buffer will be converted to UTF-8, before writing to file (ie, file gets converted and written as UTF-8 with or without BOM)
"   :set bomb                 " This toggles on the BOM for UTF-8 with BOM. If you want UTF-8 without BOM, discard this line
"
" == PLUGINS ==
" .See pathogen notes about how to install/uninstall plugins
" .See loaded plugins
"   :scriptnames
"
"   Pathogen:
"     .manage plugins instalation in modular fashion 
"     .to add a new plugin, just put it inside ~/.vim/bundle (or for win: ~\vimfiles\bundle)
"      See the USAGE section below for more info
"     .see 
"       [1] http://logicalfriday.com/2011/07/18/using-vim-with-pathogen/
"       [2] https://github.com/tpope/vim-pathogen/
"       [3] http://www.vim.org/scripts/script.php?script_id=2332
"
"     CONFIG:
" filetype off                                  " Pathogen needs to run before plugin indent on
call pathogen#runtime_append_all_bundles()
call pathogen#helptags()                        " generate helptags for everything in 'runtimepath'
filetype plugin indent on
"
"     INSTALL:
"       .see [2]
"
"     USAGE:
"       INSTALL plugins:
"           .from ZIP or plugin.vim
"                  #create plugin-own-dir inside ~/.vim/bundle . The plugin-own-dir name is you choice 
"                  #unzip into plugin-own-dir 
"                  cd ~/.vim/bundle
"                  mkdir plugin_name 
"                  cd plugin_name && unzip <plugin.zip> 
"                   
"           .from github
"                  #place each plugin-own-dir inside ~/.vim/bundle
"                  cd ~/.vim/bundle
"                  git clone git://github.com/scrooloose/nerdtree.git
"
"       UNINSTALL plugin:
"         rm -Rf ~/.vim/bundle/<plugin-own-dir>
"
"
"   Nerdtree: 
"     CONFIG:
" F4 mapping: toggle open/close NERDTree
map <F4> :NERDTreeToggle<CR>
" Show in the right
let g:NERDTreeWinPos = "right"
" Change width
let NERDTreeWinSize = 60
"
"     INSTALL:
"       cd ~/.vim/bundle && git clone git://github.com/scrooloose/nerdtree.git
"       #see also http://www.vim.org/scripts/script.php?script_id=1658 
"
"     USAGE:
"         F2                 - (mapped in this file) toggle open/close NERDTree 
"         :NERDTree          - open nerdtree
"         :NERDTreeClose     - close nedtree
"         hjkl               - navigate (move) though tree
"         o                  - open file in buffer or open/close directory
"         t                  - open file in new tab
"         i                  - open file in new horiz window
"         s                  - open file in new vert window
"         p                  - goto parent dir
"         r                  - refresh current dir
"         ma                 - create new file in current directory
"
"
"   Vimmarkdown: (vim-markdown)
"     .see https://github.com/plasticboy/vim-markdown/
"     INSTALL:
"       .cd ~/.vim/bundle && git clone https://github.com/plasticboy/vim-markdown.git
"
"
"   Surround:
"     .see http://www.vim.org/scripts/script.php?script_id=1697  
"     .see http://www.catonmat.net/blog/vim-plugins-surround-vim/
"     .adds (s)urround modifier
"     .surrounding chars:
"         " ' ` ( ) { } [ ] < >  
"
"             ( { [ with trailing space
"             ) } ] without space
"
"     INSTALL:
"      cd ~/.vim/bundle && git clone git://github.com/tpope/vim-surround.git 
"
"     USAGE:
"       .delete surroundings
"         ds<surrounding-char>
"         'Hello world'   ->  ds' = (d)elete (s)urrounding '          ->  Hello world
"
"                             Text              Command    New Text
"                             ---------------   -------    -----------
"                             'Hello W|orld'    ds'        Hello World
"                             (12|3+4*56)/2     ds(        123+4*56/2
"                             <div>fo|o</div>   dst        foo
"                             
"                             (| is the position of cursor in these examples)
"
"
"
"       .change surroundings
"         cs<old-sur-chr><new-sur-chr>
"         cs<noun><old-sur-chr><new-sur-chr>
"         where 
"           <noun> is for example:
"                       none - will automatically find the surroundings wherever they are
"                       w    - word
"                       W    - word including punctuation
"                       s    - sentence
"                       p    - paragraph
"                       t    - 'till next surrounding (represents the next surrounding, whatever it is)
"                       ...
"
"                              Text              Command    New Text
"                              ---------------   -------    -----------
"                              (Hello |world!)   cs)'       'Hello world!'
"                              (Hello |world!)   cs)<q>     <q>Hello world!</q>
"                              (123+4|56)/2      cs)]       [123+456]/2
"                              (123+4|56)/2      cs)[       [ 123+456 ]/2
"                              <div>foo|</div>   cst<p>     <p>foo</p>
"                              fo|o!             csw'       'foo'!
"                              fo|o!             csW'       'foo!'
"
"                              (| is the position of cursor in these examples)
"
"       .add surroundings 
"         its like changing an +empty+ surround, where you must precisely specify-what-to-be-surrounded
"
"         cs<noun-specifying-what-to-be-surrounded><sur-char>
"
"                 Text              Command      New Text
"                 ---------------   -------      -----------
"                 Hello w|orld!     csw)         Hello (world)!
"                 fo|o              csw<html>    <html>foo</html>
"                 foo quu|x baz     css'         'foo quux baz'         css' = (c)hange (s)urround (s)entence '
"                 
"                 (| is the position of cursor in these examples)
"         
"         When <noun-specifiyng-what-to-be-surrounded> is complicated, then
"         make use of the (v)isual:
"             <visual-select>s)
"             
"                 Text                  Command      New Text
"                 ---------------       -------      -----------
"                 |my-keys-are-missing   vEs'        |'my-keys-are-missing' 
"                
"                 
"
"
"       
"       .INSERT MODE add quick surroundings
"           <C-g>s<sur-chr>
"
"                 In INSERT MODE
"                 Text              Command           New Text
"                 ---------------   -------           -----------
"                                   <C-g>s'                '|'
"                                   <C-g>s<html>      <html>|</html> 
"
"                 (| is the position of cursor in these examples)
"
"
"  XX REMOVED supertab
"  XX    because it came up by mistake too many times when putting TABs after words
"  XX      1. Download supertab.vmb to any directory. 
"  XX      2. Open the file in vim ($ vim supertab.vmb). 
"  XX      3. Source the file (:so %).
"
"   Taglist:
"     see http://www.vim.org/scripts/script.php?script_id=273
"
"     CONFIG:
let Tlist_GainFocus_On_ToggleOpen = 1
  " goto Tlist window when openning it
"map <F3> :TlistToggle<CR>
"disabled for TagBar
  " toggle Taglist window
let Tlist_Use_Right_Window = 1
  " show in the right side
let Tlist_WinWidth = 50
  " Tlist windows width to 50
"
"     INSTALL:
"       .install exhuberant-ctags 
"         windows: just copy ctags.exe into vim/vim73)
"         linux:  sudo apt-get install exuberant-ctags 
"       .install taglist
"         copy zip from from http://www.vim.org/scripts/script.php?script_id=273 
"         cd ~/.vim/bundle
"         mkdir taglist && cd taglist
"         unzip taglist.zip
"
"     USAGE:
"       :TlistToggle      :) mapped to <F3>
"
"
"   Minibufexpl:
"     .not from vim.org, from https://github.com/fholgado/minibufexpl.vim 
"     .convenient top-bar showing buffers state and numbers
"
"     INSTALL:
"       cd ~/.vim/bundle && git clone git://github.com/fholgado/minibufexpl.vim
"
"
" --- Rubys ---
"
"   VimEndwise: (Vim-endwise)
"     .auto-adding 'end' in blocks
"     INSTALL:
"       cd ~/.vim/bundle && git clone git://github.com/tpope/vim-endwise.git
"       
"
"
"   VimTextobjRubyblock: (Vim-textobj-rubyblock)
"    - adds noun 'r' for 'ruby block'
"    CONFIG:
:runtime macros/matchit.vim 
set nocompatible 
if has("autocmd") 
  filetype indent plugin on 
endif 
"
"    INSTALL:
"     -dependencies: 
"       textobj-user  
"         cd ~/.vim/bundle && git clone git://github.com/kana/vim-textobj-user.git
"     -vim-textobj-rubyblock
"         cd ~/.vim/bundle && git clone git://github.com/nelstrom/vim-textobj-rubyblock.git
"
"    USAGE:
"     select block including head/end lines:        var = (v)isual (a)round  (r)uby-block
"     select block excluding head/end lines:        vir = (v)isual (i)nside  (r)uby-block
"
"
"  Molokai: (colorscheme plugin)
"
"   CONFIG:
" set colorscheme
"   .colors are different between Vim (worse) and Gvim (better)
"   .the best is to have a Vim-colorscheme in .vimrc, and another GVim-colorscheme in some-gvimrc-file... 
:colorscheme molokai
" enable 256 colors
set t_Co=256
" Change ruby comments color (see http://www.devdaily.com/linux/vi-vim-editor-color-scheme-syntax )
highlight Comment  term=bold ctermfg=6 ctermbg=0
"   
"   INSTALL:
"     .see http://www.vim.org/scripts/script.php?script_id=2340
"     .pathogen does not work with colorschemes (at least not with this one)
"     mkdir -p ~/.vim/colors && cp path/to/molokai.vim ~/.vim/colors
"
"   USAGE:
"     :colorscheme molokai
"
"
"
"  ArduinoVim: (arduino.vim)
"       .see http://www.vim.org/scripts/script.php?script_id=2654
"
"    INSTALL:
"       # download latest arduino.vim file from http://www.vim.org/scripts/script.php?script_id=2654
"       # and move it into ~/.vim/syntax
"       mkdir -p $HOME/.vim/syntax
"       wget -O $HOME/.vim/syntax/arduino.vim 'http://www.vim.org/scripts/download_script.php?src_id=17108'
"         
"    CONFIGURE:
autocmd! BufNewFile,BufRead *.pde setlocal ft=arduino
autocmd! BufNewFile,BufRead *.ino setlocal ft=arduino
"         
"   
"    USAGE:
"       .open a .pde or .ino file, and see the highlight
"
"
"
" ---- END PLUGINS ----




" -- restore cursor position in file --
" Uncomment the following to have Vim jump to the last position when
" reopening a file
if has("autocmd")
  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif


" dont break lines automatically (maybe some day I'll undo this... untill then, enjoy :) )
set nolinebreak
set textwidth=0

" To paste text without crazy-identation-cascading problems
"     enter insert-mode
"     press <F2>
"     <paste>
"     press <F2>
"     leave insert-mode
set pastetoggle=<F2>      " allows you to press F2 when in insert mode, to toggle 'paste' on and off. 
set showmode              " enables displaying whether 'paste' is turned on in insert mode.

" -- CLIPBOARD --
" To have clipboard read/write between VIM and Ubuntu: 
"   - apt-get install vim-gnome 
"   - in VIM use register "+ to copy/paste (ej: "+yw or v..."+y)
"   - also try to use register "* without any of the above, it might work...
"

" -- re-source .vimrc --
" :source ~/.vimrc


" Default completion will NOT go recursive into included/required files
" see http://stackoverflow.com/questions/915152/do-not-include-required-files-into-vim-omnicompletion
set complete-=i

set ignorecase        "
set smartcase         " Smartcase for searches  
                      " If you search for something containing uppercase characters, it will do a case sensitive search
                      " If you search for something purely lowercase, it will do a case insensitive search. 
                      " You can use \c (case insensitive) and \C (case sensitivo) inside the regexp to override this
                      
set nocompatible      " We're running Vim, not Vi!
syntax on             " Enable syntax highlighting
filetype on           " Enable filetype detection
filetype indent on    " Enable filetype-specific indenting
filetype plugin on    " Enable filetype-specific plugins

set expandtab
set tabstop=2 shiftwidth=2 softtabstop=2
set autoindent


" --- FOLDS ---
"   zR zM   open,close all folds
"   zcVzCza close all innerfolds and leave open the topmost-current fold
"           zc - close current fold
"           V  - enter visual-line mode
"           zC - close all  
"           za - reopen topmost-current fold 
"   z<Tab>  has a mapping :) to open outmost fold and show inner folds closed
"
"   zo zc   open,close fold     
"   zO zC   open,close fold (multilevel deep)
"
"   za      toggle fold (open/close)
"   zA      toggle open/close fold  (multilevel deep) "
"
"   zj zk   goto next,previous fold
"
"   [z ]z   goto start,end of opened folder
"                                       
"   
"
set foldmethod=syntax
":) set z<Tab> folding 
nmap z<Tab> mtzcVzCzo`t
" Don't screw up folds when inserting text that might affect them, until
" leaving insert mode. Foldmethod is local to the window. Protect against
" screwing up folding when switching between windows.
autocmd InsertEnter * if !exists('w:last_fdm') | let w:last_fdm=&foldmethod | setlocal foldmethod=manual | endif
autocmd InsertLeave,WinLeave * if exists('w:last_fdm') | let &l:foldmethod=w:last_fdm | unlet w:last_fdm | endif
" -- open most folds by default --
" foldlevelstart is the starting fold level for opening a new buffer. If
" it is set to 0, all folds will be closed. Setting it to 99 would guarantee
" folds are always open. So, setting it to 10 here ensures that only very
" nested blocks of code are folded when opening a buffer.
set foldlevelstart=10

" ----------- HIGHLIGHT ------------
" NOTE: after seing the highlighted matches, you can
" stop-showing-the-highlights by entering ':nohl'
"
" highlight all search matches
set hlsearch
set incsearch           " ...dynamically as they are typed.
nmap <F12> :nohl<CR>

" highlight current line
set cursorline          

" -- highlight matching [{()}] --
" With showmatch, when your cursor moves over a parenthesis-like character, 
" the matching one will be highlighted as well.
"set showmatch  
"  NOTE: without setting it, its already active, so it seems its activated by default

" Map-in-insert-mode jk instead of Esc
"imap jk <Esc>
"UPD: disabled as sometimes provoked a hard-to-find conflict when pasting text

" ---- Mouse-selection will automatically be COPYed into clipboard
" Method 1 (for Xwindows and mswindows), this map is different from the one shown in vim documentation:
noremap <LeftRelease> "+y<LeftRelease>
" Method 2 (works only on ms-windows vim63):
set guioptions+=a



" -- TABS --
"   vi -p <file 1> <file 2> ...<file n>
"   
"   :tabe <file>|<buff#>            works for both existing and new files
"   :tabc                           close current tab
"
"   gt            :tabn             go to next tab
"   gT            :tabp             go to previous tab
"   :tabfirst                       go to first tab
"   :tablast                        go to last tab
"   Ngt                             goto tab N (from left: 1,2,3,4... != tabm )
"
"   :tabs                           list all tabs including their displayed windows
"   :tabm 0                         move current tab to first-left (from left: 0,1,2,3...)
"   :tabm                           move current tab to last-left
"   :tabm N                         move current tab to (N+1)-left 
"   
"   - each tab can have it's own multiple windows


" -- WINDOWS --
"   CTRL-W s <file>
"   :sp      <file>           : create new window above (horizontal), and open <file> 
"
"   CTRL-W v <file> 
"   :vs      <file>           : create new window right (vertical), and open <file>
"
"   CTRL-W q  OR q            : close current window
"   CTRL-W w                  : switch to last windows
"   CTRL-W h/j/k/l            : switch to window up/down/left/right
"   CTRL-W N+                 : expand window size (N lines)
"   CTRL-W N-                 : reduce window size (N line)
"   CTRL-W _                  : maximize current window (and shrink other windows)

" -- BUFFERS --
"   :e <file>                     :(re)load <file> (file new or existing) into a buffer 
"
"   :e #<buff#>
"   <buff#> CTRL-6                :edit <buff#>  (see :ls)
"
"   :e #
"   CTRL-6                        :switch to last-buffer (alternate buffer)
"
"   :ls                           :shows buffers (each with buff#, status, filename, etc)
"   :b <file>|<buff#>             :switch to buffer <file> or <buff#>
"                                  Use TAB to autocomplete the (partial) filename, 
"                                  from the existing buffers 
"   :bdelete  <file>|<buff#>      :close buffer of <file> or <buff#>
"   CTRL-6 (= CTRL-^)             :switch to last buffer
"     - when a file is open in vim, a buffer is created to store the file, marks, etc...
"     - windows and tabs can show any buffer (use :buffer inside a window/tab to change it's visible buffer)
"     - see correct usage of buffers and tabs (unlike n++) in http://stackoverflow.com/questions/102384/using-vims-tabs-like-buffers

" Note about JUMPs and CHANGEs
"   -- JUMPS --
"     .commands that produce 'jumps's     :    searching, subtituting, marks
"     .commands that don't produce 'jump's:    scrolling, inserting, visual mode
"     .jumps list is multifile, so going to next/prev jumps you can go into different files
"
"               :jumps          : show jumps list (multifile)
"               <C-O>           : goto next jump
"               <C-I>=<Tab>     : goto prev jump  (NOTE: I have <Tab> remaped in this same file, so it will not work)
"                 NOTE: these two above are used mainly to jumpt between
"                 search results
"               ``              : goto previous position, before last jump
"
"  --  CHANGES (!= 'jump', although we can jump-changes) --
"     .a 'jump' is defined when search/replace/marks
"     .a 'change' is defined whenever a change is made: insert, replace, change, ... 
"     .we can 'jump-between-changes' (jump-changes, :changes) which is different from jumping between "jump"s (:jumps)
"     .so a 'change' migh be more usefull than a 'jump' - it sounds like 'jump's for wider movements (search/marks) and 'change's for 
"       local movements (edit, scroll, change-jump to return to last edit position, ...)
"     
"               :changes        : show changes-jumps list (current file only)
"               g;              : goto prev change (jump-change back)
"               g,              : goto next change (jump-change forward)
"               `.              : goto last change (jump-change last change)
"
" -- JUMPS && CHANGES --
"     .create marks, use `<mark> 
"     .use `. to return to last changed-position (where insert/change/replace/substitution was made)
"     .use g; to return to last changed-position



" Swap (recovery) files (xxx.txt.swp) go to /tmp instead of current dir
" Backup (xxx.txt~) go to /tmp instead of current dir
" If there are multiple users of the same linux system, writting filenames
" with same name at same time, it might give problems. In that case, it's
" better to use $HOME/tmp instead of system-wide /tmp
" In my case, that will not happen :)
" http://winterdom.com/2009/02/vimswapfiles
if has("win32") || has("win64")
  set backupdir=$TMP
  set directory=$TMP
else
  set backupdir=/tmp
  set directory=/tmp
end

" -- SESSIONS --
" :mksession[!] /path/to/session/file.vim          :save session (!overwrites) into file (defaults to Session.vim)
" :wqa                                             :saves all buffers and quits vim (write quit all)
" $ vim -S Session.vim          
" :source  Session.vim                             :load session from file 
set sessionoptions=blank,buffers,curdir,folds,globals,help,localoptions,options,resize,tabpages,winsize,winpos


" gf - resetting suffixesadd option which anoingly adds '.rb' to filenames inside a .rb file
" It may not work, as suffixesadd option is buffer-specific and so not global...
set suffixesadd=""


" -- Line numbers --
" set nonumber          " Hide line numbers
 set number            " Show absolute line numbers
" set relativenumber      " Show relative line numbers
" au FocusGained * :set relativenumber       " Show relative line number when vim gains focus
au FocusLost * :set number                 " Show absolute line number when vim looses focus





" -- :) personifications :) --
nmap º :w!<CR>
  " Save buffer (does not work with <C-S>... hm...)

nmap Y yy
  " yank hole line

nmap <C-^> :w!<CR>:e #<CR>
  " save current buffer before switching between #buffers

vnoremap <C-r> "hy:%s/<C-r>h//g<left><left>
  " Visual-mode + Control-r : replace selected text in the current buffer
"vnoremap <C-R> "hy:bufdo %s/<C-r>h//g<left><left>
  " Visual-mode + Control-R : replace selected text in all buffers
  

nmap <F2> :ls<CR>:e #      " <F2> list buffers and leaves cursor ready to edit a buffnum
nmap <F5> :bp<CR>          " <F5> buffer previous
nmap <F6> :bn<CR>          " <F6> buffer next


"  From [http://items.sjbach.com/319/configuring-vim-right]
" 
"  -> Turn on hidden
"  Dont worry about the name. What this does is allow Vim to manage multiple buffers effectively.
"  The current buffer can be put to the background without writing to disk;
"  When a background buffer becomes current again, marks and undo-history are remembered.
set hidden
"
"  -> Remap ` to '
"  These are very similar keys. Typing 'a will jump to the line in the current file marked with ma. However, `a will jump to the line and column marked with ma.
"  Its more useful in any case I can imagine, but its located way off in the corner of the keyboard. The best way to handle this is just to swap them:
nnoremap ' `
nnoremap ` '
"
"  -> Map leader to ,
"  The leader character is your own personal modifier key, as g is Vim's modifier key (when compared to vi). The default leader is \, but this isn't located standardly on all keyboards and requires a pinky stretch in any case.
let mapleader=","
"  <SPACE> is also a good choice. Note: you can of course have several personal modifier keys simply by mapping a sequence, but Vim handles the leader key more formally.
"
"  -> remap j/k to gj/gk to make j/k move by virtual lines instead of physical lines (to work properly with wrapped-long-lines)
noremap <silent> <expr> j (v:count == 0 ? 'gj' : 'j')
noremap <silent> <expr> k (v:count == 0 ? 'gk' : 'k')
"
"  -> Keep a longer history
"  By default, Vim only remembers the last 20 commands and search patterns entered. It's nice to boost this up:
set history=1000
"
"  -> Enable extended % matching
"  The % key will switch between opening and closing brackets. By sourcing matchit.vim ¿ a standard file in Vim installations for years ¿ the key can also switch among e.g. if/elsif/else/end, between opening and closing XML tags, and more.
runtime macros/matchit.vim
"  Note: runtime is the same as source except that the path is relative to the Vim installation directory.
"
"  -> Make file/command completion useful
"  By default, pressing <TAB> in command mode will choose the first possible completion with no indication of how many others there might be. The following configuration lets you see what your other options are:
"set wildmenu
"  To have the completion behave similarly to a shell, i.e. complete only up to the point of ambiguity (while still showing you what your options are), also add the following:
"set wildmode=list:longest
"  
"  -> Set terminal title
set title
"
"
"  -> Disable switch bell from auditive to visual
set visualbell
" --------------------------


" ====== My functions =======
" (see http://www.ibm.com/developerworks/linux/library/l-vim-script-1/index.html)
" r ! wget https://raw.github.com/zipizap/strapdown_template/master/strapdown_template.md.html -qO-
"
" ......................
" .NewStrapdownTemplate.
" ......................
" Description:
"   Open a new empty file, and call this function
"   It will download and insert the strapdown template into the current buffer
"   After download it cleans the tag <xmp> from its default howto-contents
" Usage:
"       :e /tmp/new_template.md.html 
"       :call NewStrapdownTemplate()
"       
"
" Configuration:
function! NewStrapdownTemplate()
  r ! wget https://raw.github.com/zipizap/strapdown_template/master/strapdown_template.md.html -qO-
  silent 0d
  silent /^<title>/s/TODO: YOUR TITLE/\=expand('%')/g
  silent /^<xmp/+1;/^<\/xmp>/-2d
  nohl
endfunction
"
"
"
" ..........................
" .Arduino: arduino_load.sh.
" ..........................
" Description:
"
" Usage:
"   Edit an arduino .ino file, and press <Leader>aa to compile&&upload&&clean 
"   For this to work, setup arduino as explained in .../Dropbox/Refs/arduinos/arduino_setup_ubuntu.md.html
"
" Configuration:
map <Leader>aa :w <bar> !clear; cd "$(dirname %)"; $HOME/arduinos/cli_bin/arduino_load.sh <CR>
"
"
"
" -- Tricks --
" see [2]
"
" Usefull registers
"   see [http://blog.sanctum.geek.nz/advanced-vim-registers/]
"
"   - the ""
"       . keeps last y/Y/d/D/x/X/c/C/s/S text
"
"   - the "0
"       .keeps last y/Y yank (only for yanks!)
"
"   - the "1 "2  "3  ... "9
"       ."1 keeps last full-line dd/cc (only full-line dd or cc)
"       ."2, "3, ... "9 are the shifting history of "1 (1 > 2 > 3 ... > 9)
"       .they just apply to full-line dd/cc
"   
"   - the ".
"       .keeps the last inserted text
"
"   - the "_
"       .its like /dev/null of the registers: it will receive everything, but
"       return nothing
"
"   - the "+
"       .as configured and explained above in this file, its configured as a
"       bridge into X clipboard
"
"   - append into register "a (note lowercase!)
"       "Ay     <- use the register name in upcase, and it will not substitute
"               the register contents, it will APPEND into it
"       Example
"         VIM before        what is         what happens      reg "a content
"         executing         executed                          after execution 
"
"   1.|   ab|cd             "ax             c deleted           c 
"     |   ab|d              "ax             d deleted           d
"     |   ab|               "ap             d is pasted         d (unchanged)
"     |   abd               -------------------------------------------------
"              
"   2.|   ab|cd             "ax             c deleted           c
"     |   ab|d              "Ax             d deleted           cd        <<= reg "a got APPENDED, not overwritten, because of capital usage "A
"     |   ab|               "ap             cd is pasted        cd (unchanged) 
"     |   abcd              -------------------------------------------------
"
"
"   - the "%
"       .always contains the current filename
"
"   - show registers contents
"       :registers
"
"   - official help
"       :help registers
"   
"
" Reselect last visual selection
"   gv
"
" Search&Replace in all buffers
" :bufdo %s/this works with any vim command/-_-/g
"
" Verbs, modifiers, nouns
"     Verbs 
"                 (v)isual    
"                 (c)hange    
"                 (d)elete  
"                 (y)ank
"     
"     Modifiers
"                 (i)nside   
"                 (a)round    
"                 (s)urround, plugin surround
"                 (t)ill_char but not including the char    
"                 (f) like (t)ill char but including the char
"                 (/) like (t)ill regexp
"
"     Nouns (text-object)
"                 (w)ord 
"                 (W)ord including adjacent punctuation ( me. )
"                 (s)entence      
"                 (p)aragraph
"                 (b) block/parenthesis
"                 (t)ag
"                 (")         (') 
"
"     Examples
"                 diw                     delete inside word
"                 cis                     change inside sentence
"                 ci"                     change inside quote
"                 c/foo                   change untill next ocurrence of 'foo'
"                 ctX                     change untill letter X
"                 vap                     visual around paragraph
"                 c2w                     change 2 words
"                 
"                 csw)                    add surround (word)
"                 cs)]                    change surround (xxx) -->  [xxx]
"                 ds'                     delete surround 'xxx' -->   xxx
"                 InsertMode <C-g>s'      '|'
"
"
"
" INSERT mode
"   <C-g>s'                '|'
"   <C-g>s<html>      <html>|</html>
"
"   <C-w>             erase word backward
"   <C-u>             erase line
"
"   <C-x> <C-l>       line completion
"
"

" -- Vim Variables --
"  Assign variable myvar : let
"    let myvar="me\n"
"  Read variable myvar : set <var>?
"    set myvar?
"
" -- Separate multiple vim commands with | (or <bar> inside .vimrc)
"
"
" -- INFOS --
" [1] http://blog.interlinked.org/tutorials/vim_tutorial.html
"     good introduction, with explained examples
"
" [2] http://web.cs.swarthmore.edu/help/vim/markers.html
"   soft learning the basics
"
" [3] http://yanpritzker.com/2011/12/16/learn-to-speak-vim-verbs-nouns-and-modifiers/
"   vim as verbs, nouns and modifiers
"       learn some verbs: v (visual), c (change), d (delete), y (yank/copy). these are the most important. there are others
"       learn some modifiers: i (inside), a (around), t (till..finds a character), f (find..like till except including the char), / (search..find a string/regex)
"       learn some text objects: w (word), s (sentence) p (paragraph) bothers
"       see also :h text-objects    iw, aw, ib (), iB {}, ...
"
" #Tuts
" [4] http://www.openvim.com/tutorial.html
" [5] http://www.knowvim.com/
" vimgolf
"
" #History my friends
" [6] http://stackoverflow.com/questions/1218390/what-is-your-most-productive-shortcut-with-vim

" Vim colors with Putty:
"   Window / Colours
"    [x] Allow terminal to specify ANSI colours 
"    [x] Allow terminal to specify xterm 256-colour mode 
"    [x] Bolded text is a different colour 
"  Connection / Data
"    Terminal-type string: xterm-256color

"" Tabs as spaces
"" See https://stackoverflow.com/a/234578
filetype plugin indent on
" show existing tab with 4 spaces width
set tabstop=2
" " when indenting with '>', use 4 spaces width
set shiftwidth=2
" " On pressing tab, insert 4 spaces
set expandtab


" " ======= Go ========
" "
" "   ,b             build  (includes auto-save, auto-imports)
" "   ,r             run    (includes auto-save, auto-imports)
" "     ,n/m           next/previous error
" "   ,t             test
" "
" "   if             *inside function* surround (excludes func sig)
" "   af             *all function* surround (includes also func sig
" "                  ex:     vif  y
" "                          daf
" "
" "   ]]  [[         jump to next or prev function
" "   ,d or gd       jump to declaration of variable/type under cursor 
" "   ,D			       filter function/type and jump to declaration
" "   c-t or c-o     jump back 
" "
" "   K              over a type/func, show :GoDoc
" "
" "   --- splitjoin ---
" "   (over a { )          
" "     gS      *Split* struct into multiple-lines
" "     gJ      *Join* struct into one-line
" "
" "   --- snippets: YouCompleteMe + ultisnips ---
" "     In insert-mode (!!NON-PASTE!!) type normally and options will pop up: 
" "     NOTE: does not work in *insert-mode (paste)*, only in *insert-mode (non-paste)*
" "       - c-n  c-p      cycle snippets with C-n/p 
" "       - TAB           select snipper with TAB
" "       - (write)       ignore snippets with *continue-writting*
" "     Once a snippet is selected:
" "       - c-j c-k       jump between *snippet-fields*
" "
" "            Usefull go snippets:
" "            https://github.com/fatih/vim-go/blob/master/gosnippets/UltiSnips/go.snippets
" "       
" "       									anon 			fn := func() { ... }
" "       									ap= 			a = append(a, value)
" "       
" "       									fn        fmt.Println(...)
" "       									ln        log.Println(...)
" "
" "                         errn
" "                         for forr 
" "                         gof
" "
" "       									VISUAL
" "       									ff        fmt.Printf("v = ",v)
" "       
" call plug#begin()
" Plug 'fatih/vim-go', { 'do': ':GoInstallBinaries' }
" Plug 'AndrewRadev/splitjoin.vim'
" Plug 'SirVer/ultisnips'
" Plug 'Valloric/YouCompleteMe'
" Plug 'ctrlpvim/ctrlp.vim'
" call plug#end()
" " :source $MYVIMRC
" " :PlugInstall
" 
" "
" " auto-save when calling GoBuild
" set autowrite
" " :GoBuild %  
" autocmd FileType go nmap <leader>b  :!clear<CR>:GoBuild %<CR>
" " :GoRun %
" "autocmd FileType go nmap <leader>r  <Plug>(go-run)
" autocmd FileType go nmap <leader>r  :!clear<CR>:GoRun %<CR>
" " :GoTest %
" autocmd FileType go nmap <leader>t  :!clear<CR>:GoTest %<CR>
" " jump between errors in quickfix list
" autocmd FileType go map <leader>m :cnext<CR>
" autocmd FileType go map <leader>n :cprevious<CR>
" autocmd FileType go nnoremap <leader>a :cclose<CR>
" " ?? Some people prefer to use only quickfix though. If you add the following to your vimrc all lists will be of type quickfix:
" " ?? seems simpler?
" let g:go_list_type = "quickfix"
" "
" "
" " Fix add/remove the imports automatically
" "   :GoImports
" "   Make it happen automatically when saving file
" let g:go_fmt_command = "goimports"
" "
" "
" " - Manual helpers
" " :GoImport <pckgToAddToImports>
" " :GoImport strings
" " :GoImport s<TAB>   << autocompletion
" "
" " :GoDrop <pckgToRemoveFromImports>
" " 
" " YouCompleteMe - select with *down*
" let g:ycm_key_list_select_completion = ['<Down>']
" "
" " 
" " Highlight types (disable if slowing vim)
" "let g:go_highlight_types = 1
" " Highlight function names (disable if slowing vim)
" let g:go_highlight_functions = 1
" autocmd FileType go map <leader>d :GoDef<CR>
" autocmd FileType go map <leader>D :GoDecls<CR>
" " :) zz fold everything except current position
" autocmd FileType go noremap zz mZ ggVGzC `Z zoh
" autocmd FileType go map º :w<CR>zz 
" " automatically show func signature on status-line, when cursor is moved over func
" let g:go_auto_type_info = 1
" set updatetime=100
" " Highlight var-under-cursor wherever it appears
" let g:go_auto_sameids = 1
" 
" " === gotags + tagbar ===
" " --- gotags ---
" "    Install: 
" "       sudo apt-get install ctags
" "       cd ~/bin && go build github.com/jstemmer/gotags
" "       # assure ~/bin is in your PATH
" "
" "
" "       REFS: see https://github.com/jstemmer/gotags
" "    Config:
" "
" " --- tagbar ---
" "    Install:
" "      REFS: http://majutsushi.github.io/tagbar/
" "      cd ~/.vim/bundle && wget 'https://github.com/majutsushi/tagbar/tarball/v2.7' -O /tmp/v2.7.tgz && tar xf /tmp/v2.7.tgz
" "   Config:
" "      See https://github.com/majutsushi/tagbar/blob/master/doc/tagbar.txt
" " auto-width
" " let g:tagbar_zoomwidth = 0
" "   Does not seem to work... so a dirty workaround is to make it fixed
" let g:tagbar_width = 100
" " autoclose after selecting tag
" let g:tagbar_autoclose = 1
" nnoremap <F3> :TagbarToggle<CR><C-W>l
" " tagber integration with gotags
" let g:tagbar_type_go = {
"   \ 'ctagstype' : 'go',
"   \ 'kinds'     : [
"     \ 'p:package',
"     \ 'i:imports:1',
"     \ 'c:constants',
"     \ 'v:variables',
"     \ 't:types',
"     \ 'n:interfaces',
"     \ 'w:fields',
"     \ 'e:embedded',
"     \ 'm:methods',
"     \ 'r:constructor',
"     \ 'f:functions'
"   \ ],
"   \ 'sro' : '.',
"   \ 'kind2scope' : {
"     \ 't' : 'ctype',
"     \ 'n' : 'ntype'
"   \ },
"   \ 'scope2kind' : {
"     \ 'ctype' : 't',
"     \ 'ntype' : 'n'
"   \ },
"   \ 'ctagsbin'  : 'gotags',
"   \ 'ctagsargs' : '-sort -silent'
"   \ }
