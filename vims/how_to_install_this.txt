##For debian-likes:
    #install ctags
      #debians
      sudo apt-get install exuberant-ctags
      #windows
      ... there is an ctags.exe file... do something with it... (google howto)

    #for linux clipboard
      sudo apt-get install vim-gnome

    #  . symlink ~/.vimrc
    ln -s $PWD/dot_vimrc ~/.vimrc

    #  . cp dot_vim/ $HOME/.vim
    tar -C $HOME -zxvf dot_vim_dir.tgz
      # to update dot_vim_dir.tgz, do:
      rm -f dot_vim_dir.tgz && tar -C $HOME -zcvf dot_vim_dir.tgz .vim && cd .. && git add . && git commit -m 'updated dot_vim_dir.tgz' && git push

    #### ---- DEPRECATED ----
    ##  . fix dos EOL (because of dropbox??)
    # sudo apt-get install tofrodos
    # find ~/.vim/* -exec fromdos \{\} \;




