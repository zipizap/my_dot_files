#!/usr/bin/env bash
# Paulo Aleixo Campos
__dir="$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)"
function shw_info { echo -e '\033[1;34m'"$1"'\033[0m'; }
function error { echo "ERROR in ${1}"; exit 99; }
trap 'error $LINENO' ERR
#exec > >(tee -i /tmp/$(date +%Y%m%d%H%M%S.%N)__$(basename $0).log ) 2>&1
PS4='████████████████████████${BASH_SOURCE}@${FUNCNAME[0]:-}[${LINENO}]>  '
set -o errexit
set -o pipefail
set -o nounset
set -o xtrace

main() {
  OrigArgs="${@}"
  VSCT_EditorFile="${1?missing required arg}" ; shift 1
# COMMIT_MESSAGE="${1?missing required arg}" ; shift 1

  # cd dir-of-VSCT_EditorFile
  cd "$(dirname ${VSCT_EditorFile})"

  git add -A
  git status
  read -ep "Commit message (Ctrl-c abort): " COMMIT_MESSAGE
  git commit -m "${COMMIT_MESSAGE}"
  git pull --rebase
  git push

  set +x
  shw_info "== OK - Finished successfully =="
}
main "${@}"
