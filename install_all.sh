#!/usr/bin/env bash
# Paulo Aleixo Campos
__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
__dbg_on_off=on  # on off
function shw_info { echo -e '\033[1;34m'"$1"'\033[0m'; }
function error { echo "ERROR in ${1}"; exit 99; }
trap 'error $LINENO' ERR
function dbg { [[ "$__dbg_on_off" == "on" ]] || return; echo -e '\033[1;34m'"dbg $(date +%Y%m%d%H%M%S) ${BASH_LINENO[0]}\t: $@"'\033[0m';  }
#exec > >(tee -i /tmp/$(date +%Y%m%d%H%M%S.%N)__$(basename $0).log ) 2>&1
set -o errexit
  # NOTE: the "trap ... ERR" alreay stops execution at any error, even when above line is commente-out
set -o pipefail
set -o nounset
set -o xtrace


install_many_bins() {

  # terraform
  ( cd $(mktemp -d) && curl -sL https://releases.hashicorp.com/terraform/1.1.7/terraform_1.1.7_linux_amd64.zip -o o.zip && unzip -x o.zip && chmod +x terraform && mv terraform ~/bin/terraform_1.1.7 )

  # terragrunt
  curl -sL -o ~/bin/terragrunt_0.42.8 https://github.com/gruntwork-io/terragrunt/releases/download/v0.42.8/terragrunt_linux_amd64 && chmod +x ~/bin/terragrunt_0.42.8

  # yq
  curl -Lo ~/bin/yq https://github.com/mikefarah/yq/releases/download/v4.30.7/yq_linux_amd64 
  chmod +x ~/bin/yq

  # docker
  # https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository
  sudo apt-get update
  sudo apt-get install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
  sudo mkdir -p /etc/apt/keyrings
  curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
 echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
  sudo apt-get update
  sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin
  sudo docker run hello-world
  sudo usermod -aG docker $USER
  newgrp docker
  docker run hello-world

  # az cli
  # https://learn.microsoft.com/en-us/cli/azure/install-azure-cli-linux?pivots=apt
  curl -sL https://aka.ms/InstallAzureCLIDeb | sudo bash
  sudo curl -Lo /etc/bash_completion.d/azure-cli https://raw.githubusercontent.com/Azure/azure-cli/dev/az.completion

  # helm-docs
  (cd ~/bin && curl -sL https://github.com/norwoodj/helm-docs/releases/download/v1.11.0/helm-docs_1.11.0_Linux_x86_64.tar.gz | tar xzvf - helm-docs)

  # smug (tmux-in-golang)
  (cd ~/bin && curl -sL https://github.com/ivaaaan/smug/releases/download/v0.3.3/smug_0.3.3_Linux_x86_64.tar.gz | tar xzvf - smug )



}

cd "${__dir}"

sudo apt install -y git curl vim bat tmux

# home_bin
ln -sv $PWD/home_bin ~/bin
mkdir -p ~/configs/binPrivate || true
ln -sv ~/configs/binPrivate ~/bin/private



# bash
( cd bash && ./append_to_bashrc.sh )

# git
( cd gits && ./append_to_gitconfig.sh )

# k8s
( cd k8s && ./install_kubectl_helm_k9s_istioctl.sh )

# # python
# ( cd python && ./install_this.sh)

# # ruby
# ( cd rubys && ln -sfv $PWD/.irbrc ~/.irbrc )

# tmux
( cd tmux && ln -sfv $PWD/dot_tmux.conf ~/.tmux.conf && ./add_tmux-alias_in_bashrc.sh )

# vim
( cd vims && ln -sfv $PWD/dot_vimrc ~/.vimrc && tar -C $HOME -zxvf dot_vim_dir.tgz )

# golang
( cd golang && ./install.sh )

# vscode
echo ">>>>> vscode must be installed manually (not yet automated)"

# zsh
( cd zsh && ./install.sh )

