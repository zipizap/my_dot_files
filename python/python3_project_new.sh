#!/usr/bin/env bash
# Paulo Aleixo Campos
__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
function shw_info { echo -e '\033[1;34m'"$1"'\033[0m'; }
function error { echo "ERROR in ${1}"; exit 99; }
trap 'error $LINENO' ERR
PS4='████████████████████████${BASH_SOURCE}@${FUNCNAME[0]:-}[${LINENO}]>  '
set -o errexit
set -o pipefail
set -o nounset
shopt -s inherit_errexit
set -o xtrace

main() {
  NEW_PROJ="${1?Usage: $0 myNewProj}"
  NEW_VENV_SUBDIR="venv"
  mkdir -p "${NEW_PROJ}"
  cd "${NEW_PROJ}"
  
  ### venv
  # You can install packages (or upgrade pip) within this virtualenv, and all such installs will be completely contained in that virtualenv. 
  # This also means that you can use different versions of packages for different projects, and anything that's on PyPi is available.
  
  # create venv
  python3.10 -m venv "${NEW_VENV_SUBDIR}"
  
  # daily: use venv
  cat > venv.source <<EOT
source "${NEW_VENV_SUBDIR}"/bin/activate
pip() {
  python3.10 -m pip "\${@}"
}
venv() {
  python3.10 -m venv "\${@}"
}
EOT
  source ./venv.source

  pip install ptpython
  
  cat <<EOT
================ Complete successfully =================
+ Created root-dir '${PWD}' 
+ venv: Created venv subdir, and "venv.source" to easily (re)load this venv
+ pip install ptpython
---- Daily Usage ----
# Load environment
cd $PWD && source venv.source
# Use python3.10, pip, venv etc
pip list
venv -h
ptpython --vi
python3.10 myprog.py
...
EOT
  
  # # remove venv
  # rm -r "${NEW_VENV_SUBDIR}"

}
main "${@}"


