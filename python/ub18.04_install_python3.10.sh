set -o errexit
set -o pipefail
set -o nounset
set -o xtrace

install_from_ppa_python310_pip_venv() {
  sudo add-apt-repository ppa:deadsnakes/ppa
  sudo apt update
  sudo apt install -y python3.10
  sudo apt install -y python3.10-distutils
  git clone https://github.com/pypa/setuptools.git && cd setuptools && sudo python3.10 setup.py install
  curl -sS https://bootstrap.pypa.io/get-pip.py | python3.10
  sudo apt install python3.10-venv
  # freeplane:/%20/home/paulo/Seafile/SharedUnsec/Refs/python/python.mm#ID_576142673
}


install_from_ppa_python310_pip_venv
echo ">>>>> Completed successfully $0"
echo ">>>>> You can now use: python3.10"

