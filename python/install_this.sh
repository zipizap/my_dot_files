#!/usr/bin/env bash
# Paulo Aleixo Campos
__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
function shw_info { echo -e '\033[1;34m'"$1"'\033[0m'; }
function error { echo "ERROR in ${1}"; exit 99; }
trap 'error $LINENO' ERR
PS4='████████████████████████${BASH_SOURCE}@${FUNCNAME[0]:-}[${LINENO}]>  '
set -o errexit
set -o pipefail
set -o nounset
shopt -s inherit_errexit
set -o xtrace

main() {
  cd "${__dir}"
  mkdir -p ~/bin
  #cp -v ./python3_project_new.sh ~/bin
  ln -sfv "${PWD}"/python3_project_new.sh ~/bin
  mkdir -p ~/.config/ptpython
  ln -sfv "${PWD}"/ptpython/config.py ~/.config/ptpython/config.py

  cat <<EOT
All done!
 + To install python3.10 in ubuntu18.04, have a look at '${__dir}/ub18.04_install_python3.10.sh'
 + To help scafold new-python-projects, use '~/bin/python3_project_new.sh myNewProj'
EOT

}
main "${@}"



