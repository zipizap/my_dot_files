#!/usr/bin/env bash
# Paulo Aleixo Campos
__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
function shw_info { echo -e '\033[1;34m'"$1"'\033[0m'; }
function error { echo "ERROR in ${1}"; exit 99; }
trap 'error $LINENO' ERR
#exec > >(tee -i /tmp/$(date +%Y%m%d%H%M%S.%N)__$(basename $0).log ) 2>&1
PS4='________________________${BASH_SOURCE}@${FUNCNAME[0]:-}[${LINENO}]>  '
set -o errexit
set -o pipefail
set -o nounset
shopt -s inherit_errexit
set -o xtrace

common() {
  cd "${__dir}"

  # GLOBAL VARS
  MYVAR1=zzz
  MYVAR2=xxx

}

main() {
  # parse args - ARGx will be "" or have its value
  local OrigArgs="${@}"
  common

  GO_VERSION=go1.17.6
  GO_VERSION=go1.19.4
  cd $(mktemp -d)
  curl -LO https://go.dev/dl/"${GO_VERSION}".linux-amd64.tar.gz
  sudo rm -rf /usr/local/go || true
  sudo tar -C /usr/local -xzf "${GO_VERSION}".linux-amd64.tar.gz
  if ! cat ~/.bashrc | grep '^# golang'
  then
    mkdir -p ~/configs/golangs
    cat >> ~/.bashrc <<'EOT'
# golang
export GOPATH=~/configs/golangs
export PATH=$PATH:/usr/local/go/bin:$GOPATH/bin
which go &>/dev/null && source <(go env| sed 's/^/export /g')
EOT
  fi

  shw_info "
== $(basename $0) ${OrigArgs}
== Finished successfully =="

}
main "${@}"

