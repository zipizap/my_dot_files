```
cd $HOME && mkdir configs && cd configs && git clone https://gitlab.com/zipizap/my_dot_files.git
```

If you make changes and want to `push` back, you will need to update the remote origin url with:
```
git remote set-url origin git@gitlab.com:zipizap/my_dot_files.git
```

Any comments and suggestions are welcome :)
