#!/bin/bash
grep "^alias 'tmux'" ~/.bashrc &>/dev/null || cat >>~/.bashrc  <<EOT

# :) tmux alias
alias 'tmux'='TERM=screen-256color tmux'
EOT

echo " Added tmux alias for this user in $HOME/.bashrc"
echo " If you want the tmux alias for all new users, add it manually into /etc/skel/.bashrc" 
