package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"net"

	"github.com/atotto/clipboard"
)

func ReadToEOF(ioR io.Reader) (rxBytes []byte, err error) {
	chunk_size := 1024
	someBytes := make([]byte, chunk_size)
	br := bufio.NewReader(ioR)
	for {
		//someBytes := make([]byte, 1024)
		n, err := br.Read(someBytes)
		rxBytes = append(rxBytes, someBytes[:n]...)
		//log.Println(string(someBytes[:n]), n, err)
		if err != nil {
			if err == io.EOF {
				return rxBytes, nil
			} else {
				return rxBytes, err
			}
		}
	}
}

func main() {
	ipPort := "127.0.0.1:8377"
	var err error
	var ln net.Listener
	{
		ln, err = net.Listen("tcp", ipPort)
		if err != nil {
			log.Fatal(err)
		}
		defer ln.Close()
	}

	for {
		log.Println(">> Waiting for connection on", ipPort)
		var conn net.Conn
		{
			conn, err = ln.Accept()
			if err != nil {
				log.Fatal(err)
			}
		}

		// read  untill EOF or error
		var rxString string
		{
			var rxBytes []byte
			rxBytes, err = ReadToEOF(conn)
			if err != nil {
				log.Fatal(err)
			}
			err = conn.Close()
			if err != nil {
				log.Fatal(err)
			}
			rxString = string(rxBytes)
		}
		log.Println(fmt.Sprintf("\n%s", rxString))

		// copy to clipboard
		{
			err = clipboard.WriteAll(rxString)
			if err != nil {
				log.Fatal(err)
			}
		}
	}
}
