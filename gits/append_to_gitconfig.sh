#/bin/bash
TARGET_CONFIG_FILE="$HOME/.gitconfig"
grep '^\[color\]' $TARGET_CONFIG_FILE &> /dev/null || cat >> $TARGET_CONFIG_FILE <<'EOT'

#
# ...
# more info, see man git-config

[alias]
	co = checkout
        tree = log --graph --pretty=oneline --decorate --all
        wdiff = diff --color-words
[color]
	ui = auto
	status = auto
	branch = auto
[merge]
	#ff = no
	# same as argument "--no-ff", see http://stackoverflow.com/a/14865661
	# UPDATE: this makes too much messy Merges from remote with local... lets disable for now...
EOT

echo "Updated $TARGET_CONFIG_FILE"

